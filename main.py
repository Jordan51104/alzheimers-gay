import urwid
import time

program_start = time.time()

global note_timer
global counter
counter = 0 



def read_notes():
    file = open("notes.txt", "r")
    file_read = file.readlines()

    global notes
    notes = []


    for line in file_read:
        
        i = 1
        y = []
        line_split = line.split(" ")
        notes.append(" ".join(line_split[1:]))
        if (line_split[0] == "15"):
            while (i < len(line_split)):
                y.append(line_split[i])
                i = i + 1
            notes_15s.append(" ".join(y))
        elif (line_split[0] == "30"):
            notes_30s.append(line_split[1])
        elif (line_split[0] == "45"):
            notes_45s.append(line_split[1])
        elif (line_split[0] == "60"):
            notes_1m.append(line_split[1])
        elif (line_split[0] == "120"):
            notes_2m.append(line_split[1])
        elif (line_split[0] == "180"):
            notes_3m.append(line_split[1])
        elif (line_split[0] == "240"):
            notes_4m.append(line_split[1])
        elif (line_split[0] == "300"):
            notes_5m.append(line_split[1])
        elif (line_split[0] == "360"):
            notes_6m.append(line_split[1])
        elif (line_split[0] == "420"):
            notes_7m.append(line_split[1])
        elif (line_split[0] == "480"):
            notes_8m.append(line_split[1])
        elif (line_split[0] == "540"):
            notes_9m.append(line_split[1])
        elif (line_split[0] == "600"):
            notes_10m.append(line_split[1])

    file.close()
    notes_text = urwid.Text(("".join(notes)), align='center')
    notes_text.set_text("".join(notes))

notes_15s = []
notes_30s = []
notes_45s = []
notes_1m =  []
notes_2m =  []
notes_3m =  []
notes_4m =  []
notes_5m =  []
notes_6m =  []
notes_7m =  []
notes_8m =  []
notes_9m =  []
notes_10m = []

read_notes()

reminder_list = []

def notes_collector():
    global next_notes
    next_notes = []
    for note in notes:
        reminder_list.append(note)
    global reminder_list_str
    reminder_list_str = "\n".join(reminder_list)

def throw_reminder(a, b):
    loop.set_alarm_in(15, throw_reminder)
    read_notes()
    global counter
    counter += 1
    next_notes = []
    x = 0
    for note in notes_15s:
        next_notes.append(note)
    if (counter % 2 == 0): #30s reminders
        for note in notes_30s:
            next_notes.append(note)
    elif (counter % 3 == 0): #45s reminders
        for note in notes_45s:
            next_notes.append(note)

    reminder_RS.set_text(" ".join(next_notes))
    loop.widget = reminderScreen
    x = 0
    next_notes = []
def numbered_notes():
    throwaway = []
    z = 1
    
    for item in notes:
        throwaway.append(str(z) + "   ")
        throwaway.append(item)
        z = z + 1
    global text_DS_formatted
    text_DS_formatted = "".join(throwaway)

numbered_notes()

def exit_on_q(key):
    if key in ('q', 'Q'):
        raise urwid.ExitMainLoop()

def exit(button):
    raise urwid.ExitMainLoop()

def chgvw_MS(button):
    read_notes()
    numbered_notes()
    loop.widget = mainScreen
    loop.draw_screen()

def chgvw_DS(button):
    read_notes()
    numbered_notes()
    notes_DS.set_text(text_DS_formatted)
    loop.widget = deleteNote
    loop.draw_screen()

def chgvw_NN(button):
    read_notes()
    numbered_notes()
    loop.widget = newNote
    loop.draw_screen()

def make_note(button):
    file = open("notes.txt", 'a')
    file.write('%s' % str(note_timer))
    file.write(" ")
    file.write("\n")
    file.close()
    read_notes()
    loop.widget = mainScreen
    loop.draw_screen()
    edit_NN.set_edit_text("")

def del_note(button):
    delnum = edit_DS.get_edit_text()
    delnum = int(delnum) - 1
    file = open("notes.txt", "r+")
    del_line = file.readlines()[delnum]
    file.seek(0)
    d = file.readlines()
    file.seek(0)
    for i in d:
        if i != del_line:
            file.write(i)
    file.truncate()
    read_notes()
    numbered_notes()
    notes_DS.set_text(text_DS_formatted)

def update():
    read_notes()
    numbered_notes()
    notes_collector()
    loop.widget = blankScreen
    loop.draw_screen()
    notes_text.set_text("".join(notes))
    loop.widget = mainScreen

def update_with_button(button):
    read_notes()
    numbered_notes()
    notes_collector()
    loop.widget = blankScreen
    loop.draw_screen()
    notes_text.set_text("".join(notes))
    loop.widget = mainScreen

    loop.set_alarm_in(15, reminders_15s)

def set_time_15s(button):
    note_contents = edit_NN.get_edit_text()
    file = open("notes.txt", 'a')
    if (note_contents == "" or note_contents == " "):
        old_label = button_15s.label
        button_15s.set_label("You didn't input a note!")
        loop.draw_screen()
        time.sleep(1)
        button_15s.set_label(old_label)
    else:
        file.write("15 ")
        file.write("%s" % note_contents)
        file.write("\n")
        file.close()
        update()
    loop.widget = mainScreen
    loop.draw_screen()
def set_time_30s(button):
    note_contents = edit_NN.get_edit_text()
    file = open("notes.txt", 'a')
    if (note_contents == "" or note_contents == " "):
        old_label = button_30s.label
        button_30s.set_label("You didn't input a note!")
        loop.draw_screen()
        time.sleep(1)
        button_30s.set_label(old_label)
    else:
        file.write("30 ")
        file.write("%s" % note_contents)
        file.write("\n")
        file.close()
        update()
    loop.widget = mainScreen
    loop.draw_screen()

def set_time_45s(button):
    note_contents = edit_NN.get_edit_text()
    file = open("notes.txt", 'a')
    if (note_contents == "" or note_contents == " "):
        old_label = button_45s.label
        button_45s.set_label("You didn't input a note!")
        loop.draw_screen()
        time.sleep(1)
        button_45s.set_label(old_label)
    else:
        file.write("45 ")
        file.write("%s" % note_contents)
        file.write("\n")
        file.close()
        update()
    loop.widget = mainScreen
    loop.draw_screen()

def set_time_1m(button):
    note_contents = edit_NN.get_edit_text()
    file = open("notes.txt", 'a')
    if (note_contents == "" or note_contents == " "):
        old_label = button_1m.label
        button_1m.set_label("You didn't input a note!")
        loop.draw_screen()
        time.sleep(1)
        button_1m.set_label(old_label)
    else:
        file.write("60 ")
        file.write("%s" % note_contents)
        file.write("\n")
        file.close()
        update()
    loop.widget = mainScreen
    loop.draw_screen()

def set_time_2m(button):
    note_contents = edit_NN.get_edit_text()
    file = open("notes.txt", 'a')
    if (note_contents == "" or note_contents == " "):
        old_label = button_2m.label
        button_2m.set_label("You didn't input a note!")
        loop.draw_screen()
        time.sleep(1)
        button_2m.set_label(old_label)
    else:
        file.write("120 ")
        file.write("%s" % note_contents)
        file.write("\n")
        file.close()
        update()
    loop.widget = mainScreen
    loop.draw_screen()

def set_time_3m(button):
    note_contents = edit_NN.get_edit_text()
    file = open("notes.txt", 'a')
    if (note_contents == "" or note_contents == " "):
        old_label = button_3m.label
        button_3m.set_label("You didn't input a note!")
        loop.draw_screen()
        time.sleep(1)
        button_3m.set_label(old_label)
    else:
        file.write("180 ")
        file.write("%s" % note_contents)
        file.write("\n")
        file.close()
        update()
    loop.widget = mainScreen
    loop.draw_screen()

def set_time_4m(button):
    note_contents = edit_NN.get_edit_text()
    file = open("notes.txt", 'a')
    if (note_contents == "" or note_contents == " "):
        old_label = button_4m.label
        button_4m.set_label("You didn't input a note!")
        loop.draw_screen()
        time.sleep(1)
        button_4m.set_label(old_label)
    else:
        file.write("240 ")
        file.write("%s" % note_contents)
        file.write("\n")
        file.close()
        update()
    loop.widget = mainScreen
    loop.draw_screen()

def set_time_5m(button):
    note_contents = edit_NN.get_edit_text()
    file = open("notes.txt", 'a')
    if (note_contents == "" or note_contents == " "):
        old_label = button_5m.label
        button_5m.set_label("You didn't input a note!")
        loop.draw_screen()
        time.sleep(1)
        button_5m.set_label(old_label)
    else:
        file.write("300 ")
        file.write("%s" % note_contents)
        file.write("\n")
        file.close()
        update()
    loop.widget = mainScreen
    loop.draw_screen()

def set_time_6m(button):
    note_contents = edit_NN.get_edit_text()
    file = open("notes.txt", 'a')
    if (note_contents == "" or note_contents == " "):
        old_label = button_6m.label
        button_6m.set_label("You didn't input a note!")
        loop.draw_screen()
        time.sleep(1)
        button_6m.set_label(old_label)
    else:
        file.write("360 ")
        file.write("%s" % note_contents)
        file.write("\n")
        file.close()
        update()
    loop.widget = mainScreen
    loop.draw_screen()

def set_time_7m(button):
    note_contents = edit_NN.get_edit_text()
    file = open("notes.txt", 'a')
    if (note_contents == "" or note_contents == " "):
        old_label = button_7m.label
        button_7m.set_label("You didn't input a note!")
        loop.draw_screen()
        time.sleep(1)
        button_7m.set_label(old_label)
    else:
        file.write("420 ")
        file.write("%s" % note_contents)
        file.write("\n")
        file.close()
        update()
    loop.widget = mainScreen
    loop.draw_screen()

def set_time_8m(button):
    note_contents = edit_NN.get_edit_text()
    file = open("notes.txt", 'a')
    if (note_contents == "" or note_contents == " "):
        old_label = button_8m.label
        button_8m.set_label("You didn't input a note!")
        loop.draw_screen()
        time.sleep(1)
        button_8m.set_label(old_label)
    else:
        file.write("480 ")
        file.write("%s" % note_contents)
        file.write("\n")
        file.close()
        update()
    loop.widget = mainScreen
    loop.draw_screen()

def set_time_9m(button):
    note_contents = edit_NN.get_edit_text()
    file = open("notes.txt", 'a')
    if (note_contents == "" or note_contents == " "):
        old_label = button_9m.label
        button_9m.set_label("You didn't input a note!")
        loop.draw_screen()
        time.sleep(1)
        button_9m.set_label(old_label)
    else:
        file.write("540 ")
        file.write("%s" % note_contents)
        file.write("\n")
        file.close()
        update()
    loop.widget = mainScreen
    loop.draw_screen()

def set_time_10m(button):
    note_contents = edit_NN.get_edit_text()
    file = open("notes.txt", 'a')
    if (note_contents == "" or note_contents == " "):
        old_label = button_10m.label
        button_10m.set_label("You didn't input a note!")
        loop.draw_screen()
        time.sleep(1)
        button_10m.set_label(old_label)
    else:
        file.write("600 ")
        file.write("%s" % note_contents)
        file.write("\n")
        file.close()
        update()
    loop.widget = mainScreen
    loop.draw_screen()

# main screen (no identifier) layout
txt = urwid.Text(("Your Notes:"), align='center')
notes_text = urwid.Text(("".join(notes)), align='center')
button_exit = urwid.Button("Exit")
button_del = urwid.Button("Delete Note")
button_new = urwid.Button("New Note")
button_update = urwid.Button("Update Notes")

pile = urwid.Pile([txt, notes_text, button_new, button_del, button_update, button_exit])
mainScreen = urwid.Filler(pile, 'top')

# new note (NN) screen layout
txt_NN = urwid.Text("Make New Note:", 'center')
edit_NN = urwid.Edit("What should your note say?\n", "", False)
done_NN = urwid.Button("Done")
exit_NN = urwid.Button("Exit")
notification_NN = urwid.Text("")

## time buttons
button_15s = urwid.Button("15 seconds")
button_30s = urwid.Button("30 seconds")
button_45s = urwid.Button("45 seconds")
button_1m  = urwid.Button("1 minute")
button_2m  = urwid.Button("2 minutes")
button_3m  = urwid.Button("3 minutes")
button_4m  = urwid.Button("4 minutes")
button_5m  = urwid.Button("5 minutes")
button_6m  = urwid.Button("6 minutes")
button_7m  = urwid.Button("7 minutes")
button_8m  = urwid.Button("8 minutes")
button_9m  = urwid.Button("9 minutes")
button_10m = urwid.Button("10 minutes")

buttons_NN = urwid.Pile([button_15s, button_30s, button_45s, button_1m, button_2m, button_3m, button_4m, button_5m, button_6m, button_7m, button_8m, button_9m, button_10m])

pile_NN = urwid.Pile([txt_NN, notification_NN, edit_NN, buttons_NN, exit_NN])

newNote = urwid.Filler(pile_NN, 'top')

# delete screen (DS) screen layout
txt_DS = urwid.Text("Delete Note:", 'center')
edit_DS = urwid.IntEdit("What note do you want to delete?\n")
notes_DS = urwid.Text(text_DS_formatted)
done_DS = urwid.Button("Done")
exit_DS = urwid.Button("Exit")

notification_DS = urwid.Text("")

pile_DS = urwid.Pile([txt_DS, notes_DS, edit_DS, notification_DS, done_DS, exit_DS])

deleteNote = urwid.Filler(pile_DS, 'top')

# blank screen (BS) screen layout
txt_BS = urwid.Text(" ")

pile_BS = urwid.Pile([txt_BS])

blankScreen = urwid.Filler(pile_BS, 'top')

loop = urwid.MainLoop(mainScreen, unhandled_input=exit_on_q)

loop.set_alarm_in(15, throw_reminder)

# Reminder Screen: (RS) screen layout
txt_RS = urwid.Text("Reminder:\n", 'center')
notes_collector()
reminder_RS = urwid.Text(reminder_list_str) 
notification_RS = urwid.Text("")

ok_RS = urwid.Button("Ok")

pile_RS = urwid.Pile([txt_RS, reminder_RS, notification_RS, ok_RS])

reminderScreen = urwid.Filler(pile_RS)

urwid.connect_signal(button_exit, 'click', exit)
urwid.connect_signal(button_del, 'click', chgvw_DS)
urwid.connect_signal(button_new, 'click', chgvw_NN)
urwid.connect_signal(done_NN, 'click', make_note)
urwid.connect_signal(done_DS, 'click', del_note)
urwid.connect_signal(button_update, 'click', update_with_button)
urwid.connect_signal(exit_DS, 'click', chgvw_MS)

urwid.connect_signal(button_15s, 'click', set_time_15s)
urwid.connect_signal(button_30s, 'click', set_time_30s)
urwid.connect_signal(button_45s, 'click', set_time_45s)
urwid.connect_signal(button_1m, 'click', set_time_1m)
urwid.connect_signal(button_2m, 'click', set_time_2m)
urwid.connect_signal(button_3m, 'click', set_time_3m)
urwid.connect_signal(button_4m, 'click', set_time_4m)
urwid.connect_signal(button_5m, 'click', set_time_5m)
urwid.connect_signal(button_6m, 'click', set_time_6m)
urwid.connect_signal(button_7m, 'click', set_time_7m)
urwid.connect_signal(button_8m, 'click', set_time_8m)
urwid.connect_signal(button_9m, 'click', set_time_9m)
urwid.connect_signal(button_10m, 'click', set_time_10m)
urwid.connect_signal(ok_RS, 'click', chgvw_MS)
urwid.connect_signal(exit_NN, 'click', chgvw_MS)
loop.run()
